import UIKit


enum NetworkCallError: Error {
    case InvalidUrl
    case InvalidResponse
    case SomeError
}

class NetworkFetcher {
    
    class func fetchDataWithPyramidOfDoomCallbacks() {
        
        guard let url = URL(string:"https://www.google.com") else { return }
        
        //Call - 1
        NetworkLayer.fetchDataFrom(url: url) { (data, response, error) in
            
            self.process(data, response, error, 1)
            
            //Call - 2
            NetworkLayer.fetchDataFrom(url: url) { (data, response, error) in
                
                self.process(data, response, error, 2)
                
                //Call - 3
                NetworkLayer.fetchDataFrom(url: url) { (data, response, error) in
                    
                    self.process(data, response, error, 3)
                }
            }
        }
    }
    
    private class func process(_ data: Data?, _ response:URLResponse?, _ error: Error?, _ counter: Int) {
        
        if error != nil {
            return
        }
        
        guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
            return
        }
        
        if data != nil {
            let dataAsString = String(decoding: data!, as: UTF8.self)
            print("\n\nResponse\(counter) = \(dataAsString)\n\n")
        } else {
            print("No Data Found!!!")
        }
    }
    
    class func fetchDataWithAsyncAwaitUsingDispatchGroup() {
        
        guard let url = URL(string:"https://www.google.com") else { return }
        let dispatchBackgroundQueue = DispatchQueue.global()
        let dispatchGroup = DispatchGroup()
        dispatchBackgroundQueue.async {
            
            //Call - 1
            self.fetchDataFor(url, dispatchGroup, 1)
            
            //Call - 2
            self.fetchDataFor(url, dispatchGroup, 2)
            
            //Call - 3
            self.fetchDataFor(url, dispatchGroup, 3)
        }
    }
    
    class private func fetchDataFor(_ url: URL, _ group: DispatchGroup, _ counter: Int) {
        do {
            let data = try NetworkLayer.fetchDataFrom(url: url, inGroup: group)
            let dataAsString = String(decoding: data!, as: UTF8.self)
            print("\n\nResponse\(counter) = \(dataAsString)\n\n")
        } catch {
            print("Error is \(error)")
        }
    }
    
    class func fetchDataWithAsyncAwaitUsingSemaphore() {
        
        guard let url = URL(string:"https://www.google.com") else { return }
        let dispatchBackgroundQueue = DispatchQueue.global()
        dispatchBackgroundQueue.async {
            
            //Call - 1
            self.fetchDataFor(url, 1)
            
            //Call - 2
            self.fetchDataFor(url, 2)
            
            //Call - 3
            self.fetchDataFor(url, 3)
        }
    }
    
    class private func fetchDataFor(_ url: URL, _ counter: Int) {
        do {
            let data = try NetworkLayer.fetchDataFrom(url: url)
            let dataAsString = String(decoding: data!, as: UTF8.self)
            print("\n\nResponse\(counter) = \(dataAsString)\n\n")
        } catch {
            print("Error is \(error)")
        }
    }
}


class NetworkLayer {
    
    class func fetchDataFrom(url: URL, withCompletionHandler handler: @escaping(Data?, URLResponse?, Error?)->Void) {
        
        let sessionConfiguration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: sessionConfiguration)
        session.dataTask(with: url) { (data, response, error) in
            handler(data, response, error)
        }.resume()
    }
    
    class func fetchDataFrom(url: URL) throws -> Data? {
        
        let sessionConfiguration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: sessionConfiguration)
        
        var data: Data?
        var response: URLResponse?
        var error: Error?
        
        let semaphore = DispatchSemaphore(value: 0)
        session.dataTask(with: url) { (_data, _response, _error) in

            data = _data
            response = _response
            error = _error
            
            semaphore.signal()
        }.resume()
        semaphore.wait(timeout: .distantFuture)
        
        guard let httpUrlResponse = response as? HTTPURLResponse, httpUrlResponse.statusCode == 200 else {
            throw NetworkCallError.InvalidResponse
        }
        
        if error != nil {
            throw NetworkCallError.SomeError
        }
        
        return data
    }
    
    class func fetchDataFrom(url: URL, inGroup group: DispatchGroup) throws -> Data? {
        
        let sessionConfiguration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: sessionConfiguration)
        
        var data: Data?
        var response: URLResponse?
        var error: Error?
        
        group.enter()
        session.dataTask(with: url) { (_data, _response, _error) in

            data = _data
            response = _response
            error = _error
            
            group.leave()
        }.resume()
        group.wait(timeout: .distantFuture)
        
        guard let httpUrlResponse = response as? HTTPURLResponse, httpUrlResponse.statusCode == 200 else {
            throw NetworkCallError.InvalidResponse
        }
        
        if error != nil {
            throw NetworkCallError.SomeError
        }
        
        return data
    }
}

//NetworkFetcher.fetchDataWithPyramidOfDoomCallbacks()
NetworkFetcher.fetchDataWithAsyncAwaitUsingSemaphore()
//NetworkFetcher.fetchDataWithAsyncAwaitUsingDispatchGroup()


